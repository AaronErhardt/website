function join_handler(ev) {
  open_email(ev, "Joining the Hope movement");
}

function contact_handler(ev) {
  open_email(ev, "Hope movement Initiative contact");
}

function open_email(ev, subject) {
  ev.preventDefault();
  // To avoid spam...
  const encoded_email = "YW05cGJrQm9iM0JsTFcxdmRtVnRaVzUwTG05eVp3PT0=";
  const email = atob(atob(encoded_email));
  subject = encodeURI(subject);
  window.location.href = `mailto:${email}?subject=${subject}`;
}

