# Website of the Hope movement

[hope-movement.org](https://hope-movement.org)

## Building the website locally

First, you need to install [git](https://git-scm.com/) and [zola](https://www.getzola.org/).

If you want to contribute please fork this repository and clone your fork.

Then run the following commands:

```bash
# Fork the repository
cd website
git submodule init
git submodule update
zola serve --base-url 127.0.0.1
```

Now you can simply open [127.0.0.1:1111](http://127.0.0.1:1111) in your browser.

### Changing the theme

To update the theme you also need to install [node-sass](https://github.com/sass/node-sass) via npm.

You can edit the files in `themes/Zulma/scss/` and then run the script at `themes/build_theme.sh` to update the themes.
